$(document).ready(function(){
    $('.yiicasa-btn-logoff').on('click',function(e){
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
            $(this).addClass('disabled');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: $(this).data('url'),
            data: {},
            success: function(data){
                location.reload();
            }
        });
    });
    
    $('.yiicasa-btn-refresh-summary').on('click',function(e){
        e.preventDefault();
        $('.yiicasa-btn-refresh-summary').attr('disabled', 'disabled');
        $('.yiicasa-btn-refresh-summary').addClass('disabled');
        $('.yiicasa-btn-span-albums-count').addClass('muted');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: $(this).data('url'),
            data: {},
            success: function(data){
                if (typeof data.entry_count !== 'undefined'){
                    $('.yiicasa-btn-span-albums-count').removeClass('muted');
                    $('.yiicasa-btn-span-albums-count').text(data.entry_count);
                    $('.yiicasa-btn-profile-author').text(data.author_name);
                    $('.yiicasa-btn-profile-icon').attr('src', data.icon);
                    $('.yiicasa-btn-refresh-summary').removeClass('disabled');
                    $('.yiicasa-btn-refresh-summary').attr('disabled', false);
                }
            }
        });
    });
});
