<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace patricy\yiicasa\models;

/**
 * Description of ResponseObject
 *
 * @author patricy
 */
class ResponseObject {
	private $_data;

	public function getData() {
		return $this->_data;
	}

	public function setData($data) {
		$this->_data = $data;
	}

	public static function normalizeData($data){
		foreach($data as $key=>$value){
			if (is_array($value) && count($value) == 1 && isset($value['$t'])){
				$data[$key] = $value['$t'];
			}
		}
		return $data;
	}

	public function get($attributeName){
		$result = null;
		if (isset($this->_data[$attributeName])){
			$result = $this->_data[$attributeName];
		} else {
			switch($attributeName){
				case 'streamId' :
					if (!empty($this->_data['gphoto$streamId'][0]['$t'])){
						$result = $this->_data['gphoto$streamId'][0]['$t'];
					}
					break;

				case 'media$thumbnail' :
					if (!empty($this->_data['media$group']['media$content'][0]['url'])){
						$result = $this->_data['media$group']['media$content'][0]['url'];
					}
					break;

				case 'media$content' :
					if (!empty($this->_data['media$group']['media$content'][0]['url'])){
						$result = $this->_data['media$group']['media$content'][0]['url'];
					}
					break;
				case 'gml$pos' :
					if (!empty($this->_data['georss$where']['gml$Point']['gml$pos']['$t'])){
						$result = $this->_data['georss$where']['gml$Point']['gml$pos']['$t'];
					}
					break;
                case 'gphoto$originalvideo' :

                    if (!empty($this->_data['gphoto$originalvideo'])){
                        $result = $this->_data['gphoto$originalvideo'];
                    }

                    break;
                    
                case 'sourceVideoOriginal' :
					if (!empty($this->_data['media$group']['media$content'][2]['url'])){
						$result = $this->_data['media$group']['media$content'][2]['url'];
					}
					break;
                case 'sourceVideoThumb' :
					if (!empty($this->_data['media$group']['media$content'][1]['url'])){
						$result = $this->_data['media$group']['media$content'][1]['url'];
					}
					break;
                case 'sourceVideoIcon' :
					if (!empty($this->_data['media$group']['media$content'][0]['url'])){
						$result = $this->_data['media$group']['media$content'][0]['url'];
					}
					break;
                    
                case 'sourceIcon' :
					if (!empty($this->_data['media$group']['media$content'][0]['url'])){
						$result = $this->_data['media$group']['media$content'][0]['url'];
					}
					break;
			}
		}

		return $result;
	}


}
