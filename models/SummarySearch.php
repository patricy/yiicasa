<?php

namespace patricy\yiicasa\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use patricy\yiicasa\models\Summary;

/**
 * SummarySearch represents the model behind the search form about `patricy\yiicasa\models\Summary`.
 */
class SummarySearch extends Summary
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'subtitle', 'icon', 'author_name', 'author_uri', 'generator', 'last_change', 'updated_at', 'organization'], 'safe'],
            [['entry_count', 'updated_by', 'quotalimit', 'quotacurrent', 'maxPhotosPerAlbum'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Summary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'entry_count' => $this->entry_count,
            'last_change' => $this->last_change,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'quotalimit' => $this->quotalimit,
            'quotacurrent' => $this->quotacurrent,
            'maxPhotosPerAlbum' => $this->maxPhotosPerAlbum,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'subtitle', $this->subtitle])
            ->andFilterWhere(['like', 'icon', $this->icon])
            ->andFilterWhere(['like', 'author_name', $this->author_name])
            ->andFilterWhere(['like', 'author_uri', $this->author_uri])
            ->andFilterWhere(['like', 'generator', $this->generator])
            ->andFilterWhere(['like', 'organization', $this->organization]);

        return $dataProvider;
    }
}
