<?php

namespace patricy\yiicasa\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use patricy\yiicasa\models\Album;

/**
 * AlbumSearch represents the model behind the search form about `patricy\yiicasa\models\Album`.
 */
class AlbumSearch extends Album
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title','icon', 'author_name', 'author_uri', 'generator', 'last_change', 'updated_at', 'summary_id', 'name', 'access', 'album_type', 'location', 'gml_lower_corner', 'gml_upper_corner', 'gml_pos'], 'safe'],
            [['entry_count', 'updated_by', 'numphotosremaining', 'bytes_used'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Album::find()->orderBy('published DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'entry_count' => $this->entry_count,
            'last_change' => $this->last_change,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'numphotosremaining' => $this->numphotosremaining,
            'bytes_used' => $this->bytes_used,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'published', $this->published])
            ->andFilterWhere(['like', 'icon', $this->icon])
            ->andFilterWhere(['like', 'author_name', $this->author_name])
            ->andFilterWhere(['like', 'author_uri', $this->author_uri])
            ->andFilterWhere(['like', 'summary_id', $this->summary_id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'access', $this->access])
            ->andFilterWhere(['like', 'album_type', $this->album_type])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'gml_lower_corner', $this->gml_lower_corner])
            ->andFilterWhere(['like', 'gml_upper_corner', $this->gml_upper_corner])
            ->andFilterWhere(['like', 'gml_pos', $this->gml_pos]);

        return $dataProvider;
    }
}
