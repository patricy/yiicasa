<?php

namespace patricy\yiicasa\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "yiicasa_album".
 *
 * @property string $id
 * @property string $title
 * @property string $icon
 * @property string $author_name
 * @property string $author_uri
 * @property integer $entry_count
 * @property string $last_change
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $summary_id
 * @property string $name
 * @property string $access
 * @property string $album_type
 * @property string $location
 * @property integer $numphotosremaining
 * @property integer $bytes_used
 * @property string $gml_lower_corner
 * @property string $gml_upper_corner
 * @property string $gml_pos
 * @property string $published
 *
 * @property YiicasaSummary $summary
 */
class Album extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yiicasa_album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['entry_count', 'updated_by', 'numphotosremaining', 'bytes_used'], 'integer'],
            [['last_change', 'updated_at', 'published'], 'safe'],
            [['id', 'title', 'icon', 'author_name', 'author_uri', 'summary_id', 'name', 'access', 'album_type', 'location', 'gml_lower_corner', 'gml_upper_corner', 'gml_pos'], 'string', 'max' => 255],
            [['summary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Summary::className(), 'targetAttribute' => ['summary_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'icon' => 'Icon',
            'author_name' => 'Author Name',
            'author_uri' => 'Author Uri',
            'entry_count' => 'Entry Count',
            'last_change' => 'Last Change',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'summary_id' => 'Summary ID',
            'name' => 'Name',
            'access' => 'Access',
            'album_type' => 'Album Type',
            'location' => 'Location',
            'numphotosremaining' => 'Numphotosremaining',
            'bytes_used' => 'Bytes Used',
            'gml_lower_corner' => 'Gml Lower Corner',
            'gml_upper_corner' => 'Gml Upper Corner',
            'gml_pos' => 'Gml Pos',
            'published' => 'Published',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSummary()
    {
        return $this->hasOne(YiicasaSummary::className(), ['id' => 'summary_id']);
    }

   public function getPhotos()
   {
       return $this->hasMany(Photo::className(), ['album_id' => 'id']);
   }

	public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => false,
            ]
        ];
    }
}
