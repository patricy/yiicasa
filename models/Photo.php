<?php

namespace patricy\yiicasa\models;

use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "yiicasa_photo".
 *
 * @property string $id
 * @property string $published
 * @property string $updated
 * @property string $title
 * @property string $icon
 * @property string $gphoto_access
 * @property integer $gphoto_width
 * @property integer $gphoto_height
 * @property integer $gphoto_size
 * @property string $gphoto_timestamp
 * @property string $gphoto_imageVersion
 * @property string $exif_fstop
 * @property string $exif_make
 * @property string $exif_model
 * @property string $exif_exposure
 * @property string $exif_flash
 * @property string $exif_focallength
 * @property string $exif_iso
 * @property string $exif_time
 * @property string $exif_imageUniqueID
 * @property string $gml_pos
 * @property string $album_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $sourcePath
 * @property integer $is_video
 * @property integer $gphoto_originalvideo_width
 * @property integer $gphoto_originalvideo_height
 * @property string $gphoto_originalvideo_type
 * @property string $gphoto_originalvideo_duration
 * @property string $gphoto_originalvideo_channels
 * @property string $gphoto_originalvideo_samplingrate
 * @property string $gphoto_originalvideo_videoCodec
 * @property string $gphoto_originalvideo_audioCodec
 * @property string $gphoto_originalvideo_fps
 * @property string $gphoto_videostatus
 * @property string $gphoto_streamId
 * @property string $sourceVideoOriginal
 * @property string $sourceVideoThumb
 * 
 * 
 * @property Album $album
 */
class Photo extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'yiicasa_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['published', 'updated', 'created_at', 'updated_at'], 'safe'],
            [['gphoto_width', 'gphoto_height', 'gphoto_size', 'gphoto_timestamp', 'created_by', 'updated_by', 'is_video', 'gphoto_originalvideo_width', 'gphoto_originalvideo_height'], 'integer'],
            [['id', 'title', 'icon', 'gphoto_access', 'gphoto_imageVersion', 'exif_fstop', 'exif_make', 'exif_model', 'exif_exposure', 'exif_flash', 'exif_focallength', 'exif_iso', 'exif_time', 'exif_imageUniqueID', 'gml_pos', 'album_id', 'sourcePath', 'gphoto_originalvideo_type', 'gphoto_originalvideo_duration', 'gphoto_originalvideo_channels', 'gphoto_originalvideo_samplingrate', 'gphoto_originalvideo_videoCodec', 'gphoto_originalvideo_audioCodec', 'gphoto_originalvideo_fps', 'gphoto_videostatus', 'gphoto_streamId', 'sourceVideoOriginal', 'sourceVideoThumb'], 'string', 'max' => 255],
            [['album_id'], 'exist', 'skipOnError' => true, 'targetClass' => Album::className(), 'targetAttribute' => ['album_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'published' => 'Published',
            'updated' => 'Updated',
            'title' => 'Title',
            'icon' => 'Icon',
            'gphoto_access' => 'Gphoto Access',
            'gphoto_width' => 'Gphoto Width',
            'gphoto_height' => 'Gphoto Height',
            'gphoto_size' => 'Gphoto Size',
            'gphoto_timestamp' => 'Gphoto Timestamp',
            'gphoto_imageVersion' => 'Gphoto Image Version',
            'exif_fstop' => 'Exif Fstop',
            'exif_make' => 'Exif Make',
            'exif_model' => 'Exif Model',
            'exif_exposure' => 'Exif Exposure',
            'exif_flash' => 'Exif Flash',
            'exif_focallength' => 'Exif Focallength',
            'exif_iso' => 'Exif Iso',
            'exif_time' => 'Exif Time',
            'exif_imageUniqueID' => 'Exif Image Unique ID',
            'gml_pos' => 'Gml Pos',
            'album_id' => 'Album ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'sourcePath' => 'Source Path',
            'is_video' => 'Is Video', 
            'gphoto_originalvideo_width' => 'Gphoto Originalvideo Width', 
            'gphoto_originalvideo_height' => 'Gphoto Originalvideo Height', 
            'gphoto_originalvideo_type' => 'Gphoto Originalvideo Type', 
            'gphoto_originalvideo_duration' => 'Gphoto Originalvideo Duration', 
            'gphoto_originalvideo_channels' => 'Gphoto Originalvideo Channels', 
            'gphoto_originalvideo_samplingrate' => 'Gphoto Originalvideo Samplingrate', 
            'gphoto_originalvideo_videoCodec' => 'Gphoto Originalvideo Video Codec', 
            'gphoto_originalvideo_audioCodec' => 'Gphoto Originalvideo Audio Codec', 
            'gphoto_originalvideo_fps' => 'Gphoto Originalvideo Fps', 
            'gphoto_videostatus' => 'Gphoto Videostatus', 
            'gphoto_streamId' => 'Gphoto Stream ID', 
            'sourceVideoOriginal' => 'Source Video Original', 
            'sourceVideoThumb' => 'Source Video Thumb', 
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum() {
        return $this->hasOne(Album::className(), ['id' => 'album_id']);
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ]
        ];
    }

}
