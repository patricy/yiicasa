<?php

namespace patricy\yiicasa\models;

use Yii;

/**
 * This is the model class for table "yiicasa_summary".
 *
 * @property string $id
 * @property string $title
 * @property string $subtitle
 * @property string $icon
 * @property string $author_name
 * @property string $author_uri
 * @property string $generator
 * @property integer $entry_count
 * @property string $last_change
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $quotalimit
 * @property integer $quotacurrent
 * @property integer $maxPhotosPerAlbum
 * @property string $organization
 *
 * @property YiicasaAlbum[] $yiicasaAlbums
 */
class Summary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yiicasa_summary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['entry_count', 'updated_by', 'quotalimit', 'quotacurrent', 'maxPhotosPerAlbum'], 'integer'],
            [['last_change', 'updated_at'], 'safe'],
            [['id', 'title', 'subtitle', 'icon', 'author_name', 'author_uri', 'generator', 'organization'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'subtitle' => 'Subtitle',
            'icon' => 'Icon',
            'author_name' => 'Author Name',
            'author_uri' => 'Author Uri',
            'generator' => 'Generator',
            'entry_count' => 'Entry Count',
            'last_change' => 'Last Change',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'quotalimit' => 'Quotalimit',
            'quotacurrent' => 'Quotacurrent',
            'maxPhotosPerAlbum' => 'Max Photos Per Album',
            'organization' => 'Organization',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYiicasaAlbums()
    {
        return $this->hasMany(Album::className(), ['summary_id' => 'id']);
    }
}
