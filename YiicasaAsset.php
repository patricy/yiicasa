<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace patricy\yiicasa;

use yii\web\AssetBundle;

class YiicasaAsset extends AssetBundle {

    public $sourcePath = __DIR__ . '/assets';
    public $css = [
        'css/yiicasa.css',
    ];
    public $js = [
        'js/yiicasa.js',
    ];
    public $depends = [
        'yii\authclient\widgets\AuthChoiceAsset',
        'yii\web\YiiAsset',
    ];

}
