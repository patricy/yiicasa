<?php

use yii\db\Migration;

class m170104_192846_change_quota_fields_limit3 extends Migration
{
    public function up()
    {
		$this->alterColumn('yiicasa_album', 'bytes_used', $this->bigInteger());
    }

    public function down()
    {
        echo "m170104_192846_change_quota_fields_limit3 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
