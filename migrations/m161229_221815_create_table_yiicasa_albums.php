<?php

use yii\db\Migration;

class m161229_221815_create_table_yiicasa_albums extends Migration
{
    public function up()
    {
		$this->createTable('{{yiicasa_album}}', [
            'id' => $this->string(),
			'title' => $this->string(),
			'subtitle' => $this->string(),
			'icon' => $this->string(),
			'author_name' => $this->string(),
			'author_uri' => $this->string(),
			'generator' => $this->string(),
			'entry_count' => $this->integer(),
			'last_change' => $this->dateTime(),
			'updated_at' => $this->dateTime(),
			'updated_by' => $this->integer()
        ]);

		$this->addPrimaryKey('yiicasa_album_pk', 'yiicasa_album', 'id');
    }

    public function down()
    {
        echo "m161229_221815_create_table_yiicasa_albums cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
