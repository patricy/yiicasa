<?php

use yii\db\Migration;

/**
 * Handles the creation of table `summary`.
 */
class m161228_222435_create_summary_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{yiicasa_summary}}', [
            'id' => $this->primaryKey(),
			'title' => $this->string(),
			'subtitle' => $this->string(),
			'icon' => $this->string(),
			'author_name' => $this->string(),
			'author_uri' => $this->string(),
			'generator' => $this->string(),
			'entry_count' => $this->integer(),
			'last_change' => $this->dateTime(),
			'updated_at' => $this->dateTime(),
			'updated_by' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{yiicasa_summary}}');
    }
}
