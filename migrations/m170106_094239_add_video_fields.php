<?php

use yii\db\Migration;

class m170106_094239_add_video_fields extends Migration
{
    public function up()
    {
        $this->addColumn('yiicasa_photo', 'is_video', $this->boolean());

        $this->addColumn('yiicasa_photo', 'gphoto_originalvideo_width', $this->integer());
        $this->addColumn('yiicasa_photo', 'gphoto_originalvideo_height', $this->integer());
        $this->addColumn('yiicasa_photo', 'gphoto_originalvideo_type', $this->string());
        $this->addColumn('yiicasa_photo', 'gphoto_originalvideo_duration', $this->string());
        $this->addColumn('yiicasa_photo', 'gphoto_originalvideo_channels', $this->string());
        $this->addColumn('yiicasa_photo', 'gphoto_originalvideo_samplingrate', $this->string());
        $this->addColumn('yiicasa_photo', 'gphoto_originalvideo_videoCodec', $this->string());
        $this->addColumn('yiicasa_photo', 'gphoto_originalvideo_audioCodec', $this->string());
        $this->addColumn('yiicasa_photo', 'gphoto_originalvideo_fps', $this->string());
        $this->addColumn('yiicasa_photo', 'gphoto_videostatus', $this->string());

        $this->addColumn('yiicasa_photo', 'gphoto_streamId', $this->string());
        $this->addColumn('yiicasa_photo', 'sourceVideoOriginal', $this->string());
        $this->addColumn('yiicasa_photo', 'sourceVideoThumb', $this->string());
    }

    public function down()
    {
        echo "m170106_094239_add_video_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
