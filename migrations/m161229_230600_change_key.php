<?php

use yii\db\Migration;

class m161229_230600_change_key extends Migration
{
    public function up()
    {
		$this->alterColumn('yiicasa_summary', 'id', $this->integer());
		$this->dropPrimaryKey('PRIMARY', 'yiicasa_summary');
		$this->alterColumn('yiicasa_summary', 'id', $this->string());
		$this->addPrimaryKey('yiicasa_summary_pk', 'yiicasa_summary', 'id');
    }

    public function down()
    {
        echo "m161229_230600_change_key cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
