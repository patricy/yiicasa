<?php

use yii\db\Migration;

class m170104_192427_change_quota_fields_limit extends Migration
{
    public function up()
    {
		$this->alterColumn('yiicasa_summary', 'quotalimit', $this->bigInteger());
		$this->alterColumn('yiicasa_summary', 'quotacurrent', $this->bigInteger());

    }

    public function down()
    {
        echo "m170104_192427_change_quota_fields_limit cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
