<?php

use yii\db\Migration;

class m170104_223611_photo_table extends Migration
{
    public function up()
    {
		$this->createTable('yiicasa_photo', [
			'id' => $this->string(),
			'published' => $this->dateTime(),
			'updated' => $this->dateTime(),
			'title' => $this->string(),
			'icon' => $this->string(),
			'source' => $this->string(),
			'gphoto_version' => $this->string(),
			'gphoto_position' => $this->string(),
			'gphoto_albumid' => $this->string(),
			'gphoto_access' => $this->string(),
			'gphoto_width' => $this->integer(),
			'gphoto_height' => $this->integer(),
			'gphoto_size' => $this->integer(),
			'gphoto_client' => $this->string(),
			'gphoto_timestamp' => $this->integer(),
			'gphoto_imageVersion' => $this->string(),

			'exif_fstop' => $this->string(),
			'exif_make' => $this->string(),
			'exif_model' => $this->string(),
			'exif_exposure' => $this->string(),
			'exif_flash' => $this->string(),
			'exif_focallength' => $this->string(),
			'exif_iso' => $this->string(),
			'exif_time' => $this->string(),
			'exif_imageUniqueID' => $this->string(),

			'gml_pos' => $this->string()
			]);

		$this->addPrimaryKey('yiicasa_photo_pk', 'yiicasa_photo', 'id');

    }

    public function down()
    {
        echo "m170104_223611_photo_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
