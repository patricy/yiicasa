<?php

use yii\db\Migration;

class m170105_213047_outofrange_fix extends Migration
{
    public function up()
    {
		$this->alterColumn('yiicasa_photo', 'gphoto_timestamp', $this->bigInteger());
    }

    public function down()
    {
         return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
