<?php

use yii\db\Migration;

class m170104_225315_foreign_keys extends Migration
{
    public function up()
    {
		$this->dropColumn('yiicasa_photo', 'gphoto_albumid');
		$this->addColumn('yiicasa_photo',  'album_id', $this->string());
		$this->addForeignKey('fk_yiicasa_photo_album', 'yiicasa_photo', 'album_id', 'yiicasa_album', 'id');
    }

    public function down()
    {
        echo "m170104_225315_foreign_keys cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
