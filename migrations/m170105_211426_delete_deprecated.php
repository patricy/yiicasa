<?php

use yii\db\Migration;

class m170105_211426_delete_deprecated extends Migration
{
    public function up()
    {
        
		$this->dropColumn('yiicasa_photo', 'gphoto_client');
		$this->dropColumn('yiicasa_photo', 'gphoto_version');
		$this->dropColumn('yiicasa_photo', 'gphoto_position');

		$this->dropColumn('yiicasa_photo', 'source');
		$this->addColumn('yiicasa_photo', 'soursePath', $this->string());


		$this->addColumn('yiicasa_photo', 'created_at', $this->dateTime());
		$this->addColumn('yiicasa_photo', 'created_by', $this->integer());
		$this->addColumn('yiicasa_photo', 'updated_at', $this->dateTime());
		$this->addColumn('yiicasa_photo', 'updated_by', $this->dateTime());

    }

    public function down()
    {
        echo "m170105_211426_delete_deprecated cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
