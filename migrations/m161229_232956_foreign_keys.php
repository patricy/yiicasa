<?php

use yii\db\Migration;

class m161229_232956_foreign_keys extends Migration
{
    public function up()
    {
		$this->addColumn('yiicasa_album',  'summary_id', $this->string());
		$this->addForeignKey('fk_yiicasa_album_summary', 'yiicasa_album', 'summary_id', 'yiicasa_summary', 'id');
    }

    public function down()
    {
        echo "m161229_232956_foreign_keys cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
