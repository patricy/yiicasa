<?php

use yii\db\Migration;

class m170104_075640_add_more_fields_ns extends Migration
{
    public function up()
    {
        $this->addColumn('yiicasa_summary', 'quotalimit', $this->integer());
        $this->addColumn('yiicasa_summary', 'quotacurrent', $this->integer());
        $this->addColumn('yiicasa_summary', 'maxPhotosPerAlbum', $this->integer());
        $this->addColumn('yiicasa_summary', 'organization', $this->string());
        

        $this->addColumn('yiicasa_album', 'name', $this->string());
        $this->addColumn('yiicasa_album', 'access', $this->string());
        $this->addColumn('yiicasa_album', 'album_type', $this->string());
        $this->addColumn('yiicasa_album', 'location', $this->string());
        
        $this->addColumn('yiicasa_album', 'numphotosremaining', $this->integer());
        $this->addColumn('yiicasa_album', 'bytes_used', $this->integer());
        
        
        $this->addColumn('yiicasa_album', 'gml_lower_corner', $this->string());
        $this->addColumn('yiicasa_album', 'gml_upper_corner', $this->string());
        $this->addColumn('yiicasa_album', 'gml_pos', $this->string());
        
        
    }

    public function down()
    {
        echo "m170104_075640_add_more_fields_ns cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
