<?php

use yii\db\Migration;

class m170104_212310_album_fields_correction extends Migration
{
    public function up()
    {
		$this->addColumn('yiicasa_album', 'published', $this->dateTime());

		$this->dropColumn('yiicasa_album', 'generator');
		$this->dropColumn('yiicasa_album', 'subtitle');
    }

    public function down()
    {
        echo "m170104_212310_album_fields_correction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
