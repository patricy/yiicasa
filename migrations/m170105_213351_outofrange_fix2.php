<?php

use yii\db\Migration;

class m170105_213351_outofrange_fix2 extends Migration
{
    public function up()
    {
		$this->alterColumn('yiicasa_photo', 'updated_by', $this->integer());
    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
