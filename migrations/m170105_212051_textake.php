<?php

use yii\db\Migration;

class m170105_212051_textake extends Migration
{
    public function up()
    {
		$this->dropColumn('yiicasa_photo', 'soursePath');
		$this->addColumn('yiicasa_photo', 'sourcePath', $this->string());
    }

    public function down()
    {
        echo "m170105_212051_textake cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
