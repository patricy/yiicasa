<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace patricy\yiicasa;

use yii\authclient\AuthAction;
use Yii;

class YiicasaAuthAction extends AuthAction {

    public function run() {
        if (Yii::$app->request->get($this->clientIdGetParamName) === 'picasa') {
            return $this->auth(Yii::$app->getModule('yiicasa')->getClient());
        }

        throw new NotFoundHttpException();
    }

}
