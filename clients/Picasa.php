<?php

namespace patricy\yiicasa\clients;

use yii\authclient\OAuth2;
use yii\authclient\ClientInterface;
use Yii;
use yii\authclient\InvalidResponseException;
/**
 * Description of Picasa
 *
 * @author z003nmsn
 */
class Picasa extends OAuth2 implements ClientInterface {

    /**
     * @inheritdoc
     */
    public $authUrl = 'https://accounts.google.com/o/oauth2/auth';

    /**
     * @inheritdoc
     */
    public $tokenUrl = 'https://accounts.google.com/o/oauth2/token';

    /**
     * @inheritdoc
     */
    public $apiBaseUrl = 'https://picasaweb.google.com/data';

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        $this->scope = 'https://picasaweb.google.com/data/';
        // $this->setHttpClient(YiicasaHttpClient);
    }

    protected function initUserAttributes() {

    }

    /**
     * @inheritdoc
     */
    protected function defaultName() {
        return 'picasa';
    }

    /**
     * @inheritdoc
     */
    protected function defaultTitle() {
        return 'Picasa';
    }



    public function getFeed($albumId = null, $photoId = null, $userId = 'default', $params = []) {
        return $this->callApi('feed', $albumId, $photoId, $userId, $params);
    }

    public function getEntry($albumId = null, $photoId = null, $userId = 'default', $params = []) {
        return $this->callApi('entry', $albumId, $photoId, $userId, $params);
    }

    public function callApi($apiType, $albumId = null, $photoId = null, $userId = 'default', $params = []) {
        $result = null;
        $apiAddress = $apiType . '/api/user/' . $userId;
        if (!is_null($albumId)) {
            $apiAddress.= '/albumid/' . $albumId;
        }
        if (!is_null($photoId)) {
            $apiAddress.= '/photoid/' . $albumId;
        }
      //  $apiAddress.= '?' . http_build_query($params);

        if ($this->getAccessToken()) {
            $result = $this->api($apiAddress, 'GET', $params, ['GData-Version'=>'2'] );
        }
        return $result;
    }

    protected function sendRequest($request) {

        $urlComponents = parse_url($request->getUrl());
		$data = $request->getData();
		$extrafilenamePart = '';
		$cache = false;
		if (substr($urlComponents['path'], 0, 4) == 'feed' ||  substr($urlComponents['path'], 0, 5)  == 'entry' ){
			$cache = true;
		}
		if (!empty($data)){
			foreach($data as $key=>$value){
				$extrafilenamePart.='-'.$key.'-'.$value;
			}
		}
        $filename = str_replace('/', '-', $urlComponents['path']) . $extrafilenamePart;

        if (isset($data) && isset($data['alt']) && $data['alt'] == 'json') {
            $filename.= '.json';
        }

        $fullFileName = Yii::getAlias('@runtime') . '/' . $filename;
        //tmp cache
        $lastFileChange = 0;
        if (file_exists($fullFileName)) {
            $lastFileChange = filemtime($fullFileName);
        }
        //(time() - $lastFileChange) > 60 * 60 ||

        if (!file_exists($fullFileName) || !$cache) {

			$response = $request->send();

            if (!$response->getIsOk()) {
                throw new InvalidResponseException($response, 'Request failed with code: ' . $response->getStatusCode() . ', message: ' . $response->getContent());
            }

			if ($cache){
				file_put_contents($fullFileName, $response->getContent());
			}
            $result = $response->getData();
            $result['cached'] = false;
        } else {
            $fileContent = file_get_contents($fullFileName);
            $result = json_decode($fileContent, true);
            $result['cached'] = true;
        }
        return $result;
    }

}
