<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model patricy\yiicasa\models\AlbumSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="album-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'subtitle') ?>

    <?= $form->field($model, 'icon') ?>

    <?= $form->field($model, 'author_name') ?>

    <?php // echo $form->field($model, 'author_uri') ?>

    <?php // echo $form->field($model, 'generator') ?>

    <?php // echo $form->field($model, 'entry_count') ?>

    <?php // echo $form->field($model, 'last_change') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'summary_id') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'access') ?>

    <?php // echo $form->field($model, 'album_type') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'numphotosremaining') ?>

    <?php // echo $form->field($model, 'bytes_used') ?>

    <?php // echo $form->field($model, 'gml_lower_corner') ?>

    <?php // echo $form->field($model, 'gml_upper_corner') ?>

    <?php // echo $form->field($model, 'gml_pos') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
