<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

use patricy\yiicasa\widgets\AuthButton;

/* @var $this yii\web\View */
/* @var $searchModel patricy\yiicasa\models\AlbumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Albums';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="album-index">
	<?=AuthButton::widget();?>
	<!--
    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Album', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
<?php Pjax::begin(); ?>    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => '_view'
    ]) ?>
<?php Pjax::end(); ?></div>
