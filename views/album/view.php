<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model patricy\yiicasa\models\Album */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Albums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="album-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'subtitle',
            'icon',
            'author_name',
            'author_uri',
            'generator',
            'entry_count',
            'last_change',
            'updated_at',
            'updated_by',
            'summary_id',
            'name',
            'access',
            'album_type',
            'location',
            'numphotosremaining',
            'bytes_used',
            'gml_lower_corner',
            'gml_upper_corner',
            'gml_pos',
        ],
    ]) ?>

</div>
