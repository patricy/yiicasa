<?php

namespace patricy\yiicasa\controllers;

use Yii;
use patricy\yiicasa\models\Album;
use patricy\yiicasa\models\AlbumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use patricy\yiicasa\Module as Yiicasa;
use yii\web\Response;
use patricy\yiicasa\models\Photo;
/**
 * AlbumController implements the CRUD actions for Album model.
 */
class AlbumController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Album models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AlbumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Album model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Album model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Album();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Album model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Album model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Album model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Album the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Album::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionScanAll(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $albums =  Album::find()->all();
        $result = [];
        foreach($albums as $album){
            if ($album->album_type == 'InstantUpload'){
                continue;
            }
            $result[$album->id] = $this->actionScan($album->id);
        }
        return $result;

    }

	public function actionScan($id){
		$model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;

		$maxResults = Yii::$app->getModule('yiicasa')->getMaxScanResults();
		$iterations = ceil($model->entry_count / $maxResults);
        $result = [];

        $result['album'] = $model->attributes;
        $result['maxScanResults'] = $maxResults;
        $result['iterationsCount'] = $iterations;
        $result['iterations'] = [];
        if ($model->album_type =='InstantUpload{'){
            $behabiour = Yiicasa::BEHAVIOUR_SKIP_EXISTING;
        } else {
            $behabiour = Yiicasa::BEHAVIOUR_UPDATE_OLDER;
        }

        $behabiour = Yiicasa::BEHAVIOUR_SKIP_EXISTING;
        $result['behabiour'] = $behabiour;

		for($i = 1; $i<=$iterations; $i++){
            $startIndex = ($i -1)*$maxResults+1;
            $scanResult = [];
			$scanResult['result'] = Yii::$app->getModule('yiicasa')->scanAlbum($id, $startIndex, $behabiour);
            $scanResult['iteration'] = $i;
            $scanResult['startIndex'] = $startIndex;

            $result['iterations'][] = $scanResult;
		}
        return $result;
	}

	public function actionInstant(){
		$album = Album::find()->where(['album_type'=>'InstantUpload'])->one();

		$photos = Photo::find()->where(['album_id'=>$album->id])->limit(24)->offset(0)->orderBy('published DESC')->all();


		return $this->render('instant', [
                'photos' => $photos,
            ]);
	}
}
