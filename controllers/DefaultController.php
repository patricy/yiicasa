<?php

namespace patricy\yiicasa\controllers;

use patricy\yiicasa\YiicasaAuthAction;
use yii\base\Controller;
use yii\authclient\ClientInterface;
use Yii;
use yii\web\Response;

/**
 * Description of PicasaController
 *
 * @author z003nmsn
 */
class DefaultController extends Controller {

    /** @inheritdoc */
    public function actions() {
        return [
            'auth' => [
                'class' => YiicasaAuthAction::className(),
                'successCallback' => [$this, 'authenticate']
            ],
        ];
    }

    public function authenticate(ClientInterface $client) {
        $token = $client->getAccessToken();

        if(isset($token->refresh_token)) {
            var_dump($token->refresh_token);
        }
        
        die('dd');
        //refresh_token
        //Yii::$app->getModule('yiicasa')->getSummary(true);
        return;
    }

    public function actionRefresh() {
        $summaryModel = Yii::$app->getModule('yiicasa')->getSummary(true);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return !is_null($summaryModel) ? $summaryModel->attributes : null;
    }

    public function actionLogoff() {
        Yii::$app->getModule('yiicasa')->logoff();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return true;
    }

    public function actionGetEntry() {
        $client = Yii::$app->getModule('yiicasa')->getClient();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $resp = $client->getEntry(Yii::$app->request->get('album_id'), Yii::$app->request->get('photo_id'), 'default', ['alt'=>'json']);
    }

    public function actionGetFeed() {
        $client = Yii::$app->getModule('yiicasa')->getClient();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $resp = $client->getFeed(Yii::$app->request->get('album_id'), Yii::$app->request->get('photo_id'), 'default', ['alt'=>'json']);
    }

}
