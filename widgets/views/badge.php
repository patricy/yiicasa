<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
?>
<div class="btn-group btn-group-xs" role="group" aria-label="...">
    <?=Html::button(
            Html::img($model->icon, ['style'=>'height:17px', 'class'=>'yiicasa-btn-profile-icon']) . Html::tag('span', $model->author_name, ['class'=>'yiicasa-btn-profile-author']),
            [
                'type'=>'button',
                'class'=>'btn btn-default btn-xs yiicasa-btn-profile',
                'data-url' => Url::to('/yiicasa/default/profile')
            ]);?>
    <?=Html::button(
            Html::tag('span', '', ['class'=>'glyphicon glyphicon-refresh', 'aria-hidden'=>true]) . ' ' . Html::tag('span', $model->entry_count, ['class'=>'yiicasa-btn-span-albums-count']),
            [
                'type'=>'button',
                'class'=>'btn btn-default btn-xs yiicasa-btn-refresh-summary',
                'data-url' => Url::to('/yiicasa/default/refresh')
            ]);?>
    <?=Html::button(
            Html::tag('span', '', ['class'=>'glyphicon glyphicon-off', 'aria-hidden'=>true]),
            [
                'type'=>'button',
                'class'=>'btn btn-danger btn-xs yiicasa-btn-logoff',
                'data-url' => Url::to('/yiicasa/default/logoff')
            ]);?>
</div>
