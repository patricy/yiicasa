<?php

namespace patricy\yiicasa\widgets;

use patricy\yiicasa\YiicasaAsset;
use yii\authclient\widgets\AuthChoice;
use Yii;
use yii\bootstrap\Html;
use yii\helpers\Url;
use patricy\yiicasa\models\Summary;

class AuthButton extends AuthChoice {

    /**
     * @inheritdoc
     */
    public function init() {

        YiicasaAsset::register(Yii::$app->view);
        if ($this->popupMode) {
            Yii::$app->view->registerJs("\$('#" . $this->getId() . "').authchoice();");
        }
        $this->options['id'] = $this->getId();
        echo Html::beginTag('div', $this->options);
    }

    public function getBaseAuthUrl() {
        return [Url::to('/yiicasa/default/auth')];
    }

    protected function renderMainContent() {
        if (Yii::$app->user->isGuest) {
            return null;
        }
        $iconOptions = ['class' => 'img-responsive'];
        $client = Yii::$app->getModule('yiicasa')->getClient();

        if (Yii::$app->getModule('yiicasa')->isAuthorized($client)) {
            $summary = Yii::$app->getModule('yiicasa')->getSummary();
            $result = $this->render('badge', ['model' => $summary, 'iconOptions' => $iconOptions]);
        } else {
            $result = $this->clientLink($client);
        }

        return $result;
    }

}
