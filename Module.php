<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace patricy\yiicasa;

use yii\base\Module as BaseModule;
use patricy\yiicasa\clients\Picasa;
use Yii;
use patricy\yiicasa\models\Summary;
use patricy\yiicasa\models\Album;
use patricy\yiicasa\models\ResponseObject;
use patricy\yiicasa\models\Photo;
/**
 * Description of Module
 *
 * @author z003nmsn
 */
class Module extends BaseModule {

    public $clientId;
    public $clientSecret;
    public $requestOptions = [];
    public $sessionName = 'yiicasa';

    private $_maxScanResults = 1000;

    const BEHAVIOUR_UPDATE_OLDER = 'Update existing model if its older';
    const BEHAVIOUR_SKIP_EXISTING = 'Skip existing model';
    const BEHAVIOUR_FORCE_UPDATE = 'Always update model';
    const BEHAVIOUR_DO_NOT_SAVE = 'Do not save model';

    public function getClient() {
        $picasaClient = new Picasa;
        $picasaClient->clientId = Yii::$app->getModule('yiicasa')->clientId;
        $picasaClient->clientSecret = Yii::$app->getModule('yiicasa')->clientSecret;
        $picasaClient->requestOptions = Yii::$app->getModule('yiicasa')->requestOptions;
        return $picasaClient;
    }

    public function getSummary($force = false) {
        //TODO: user can have few picasa accounts
        $summaryModel = Summary::find(['updated_by' => Yii::$app->user->id])->one();

        if (is_null($summaryModel) || $force) {
            $client = $this->getClient();
            if (is_null($summaryModel)) {
                $summaryModel = new Summary();
            }

            $summaryResponse = $client->getFeed(null,null,'default',['alt'=>'json']);
            if (is_null($summaryResponse)) {
                return null;
            }

            $summaryModel->last_change = date("Y-m-d H:i:s", strtotime($summaryResponse['feed']['updated']['$t']));
            $summaryModel->id = $summaryResponse['feed']['title']['$t'];
            $summaryModel->title = $summaryResponse['feed']['title']['$t'];
            $summaryModel->icon = $summaryResponse['feed']['icon']['$t'];
            $summaryModel->author_name = $summaryResponse['feed']['author'][0]['name']['$t'];
            $summaryModel->author_uri = $summaryResponse['feed']['author'][0]['uri']['$t'];
            $summaryModel->generator = $summaryResponse['feed']['generator']['$t'];
            $summaryModel->entry_count = count($summaryResponse['feed']['entry']);

            $summaryModel->organization = $summaryResponse['feed']['gphoto$organization']['name'];
            $summaryModel->quotalimit = $summaryResponse['feed']['gphoto$quotalimit']['$t'];
            $summaryModel->quotacurrent = $summaryResponse['feed']['gphoto$quotacurrent']['$t'];

            $summaryModel->maxPhotosPerAlbum = $summaryResponse['feed']['gphoto$maxPhotosPerAlbum']['$t'];
            if ($summaryModel->save()) {
                //saving albums
                foreach ($summaryResponse['feed']['entry'] as $value) {


                    $albumId = $value['gphoto$id']['$t'];
                    $albumModel = Album::findOne($albumId);

                    if (is_null($albumModel)) {
                        $albumModel = new Album();
                        $albumModel->id = $albumId;
                    }

                    $albumModel->last_change = date("Y-m-d H:i:s", strtotime($value['updated']['$t']));
					$albumModel->published = date("Y-m-d H:i:s", strtotime($value['published']['$t']));
                    $albumModel->title = $value['title']['$t'];
                    $albumModel->icon = $value['media$group']['media$thumbnail'][0]['url'];
                    $albumModel->author_name = $value['gphoto$nickname']['$t'];

                    $albumModel->entry_count = $value['gphoto$numphotos']['$t'];
                    if (isset($value['gphoto$numphotosremaining']['$t'])) {
                        $albumModel->numphotosremaining = $value['gphoto$numphotosremaining']['$t'];
                    }
                    if (isset($value['gphoto$bytesUsed']['$t'])) {
                        $albumModel->bytes_used = $value['gphoto$bytesUsed']['$t'];
                    }



                    if (isset($value['gphoto$albumType'])) {
                        $albumModel->album_type = $value['gphoto$albumType']['$t'];
                    }

                    $albumModel->access = $value['gphoto$access']['$t'];

                    $albumModel->name = $value['gphoto$name']['$t'];


                    $albumModel->summary_id = $summaryModel->id;




                    if ($albumModel->save()) {

                    } else {
                        echo "alb";
                        print_r($albumModel->errors);
                    }
                }
            } else {
                echo "summ";
                print_r($summaryModel->errors);
            }
        }

        return $summaryModel;
    }

    public function isAuthorized($client) {
        $result = false;
       // $client = $this->getClient();
        if ($client->getAccessToken()) {
            $result = true;
        }
        return $result;
    }

    public function logoff() {
        $client = $this->getClient();
        $client->setAccessToken(null);
    }

    public function scanAlbum($id, $startIndex, $behaviour = self::BEHAVIOUR_UPDATE_OLDER){
        $result = [];

        $client = $this->getClient();

        $resp = $client->getFeed($id, null, 'default', [
            'alt' => 'json',
            'start-index' => $startIndex,
            'max-results' => $this->getMaxScanResults()]);

 
        $countSaved = 0;
        $countNotSaved = 0;
        $countSkipped = 0;
        $result['error'] = [];
        $result['cached'] = $resp['cached'];
        if (!empty($resp['feed']['entry'])){
            foreach($resp['feed']['entry'] as $entryRaw){
                $entry = ResponseObject::normalizeData($entryRaw);
                if (isset($entry['exif$tags'])){
                    $entry = array_merge($entry, ResponseObject::normalizeData($entry['exif$tags']));
                    unset($entry['exif$tags']);
                }

                $entryObject = new ResponseObject;
                $entryObject->setData($entry);


                $photoModel = Photo::findOne($entryObject->get('gphoto$id'));
                
                if (is_null($photoModel)){
                    $photoModel = new Photo();
                    $photoModel->id = $entry['gphoto$id'];
                }

                if ($behaviour == self::BEHAVIOUR_SKIP_EXISTING && !$photoModel->isNewRecord){
                    $countSkipped++;
                    continue;
                }

                if ($behaviour == self::BEHAVIOUR_UPDATE_OLDER){
                    if ($photoModel->updated < $photoModel->updated_at){
                        $countSkipped++;
                        continue;
                    }
                }


                $photoModel->published				= date("Y-m-d H:i:s", strtotime($entryObject->get('published')));
                $photoModel->updated				= date("Y-m-d H:i:s", strtotime($entryObject->get('updated')));
                $photoModel->title					= $entryObject->get('title');
                $photoModel->album_id				= $entryObject->get('gphoto$albumid');

                $photoModel->sourcePath             = pathinfo($entryObject->get('media$content'), PATHINFO_DIRNAME);
                $photoModel->icon                   = $entryObject->get('sourceIcon');
                if (strlen($photoModel->icon) > 255){
                    $photoModel->icon = dirname($photoModel->icon) . '/' . $photoModel->title;
                }
                $photoModel->gphoto_access			= $entryObject->get('gphoto$access');
                $photoModel->gphoto_width			= $entryObject->get('gphoto$width');
                $photoModel->gphoto_height			= $entryObject->get('gphoto$height');
                $photoModel->gphoto_size			= $entryObject->get('gphoto$size');
                $photoModel->gphoto_timestamp		= $entryObject->get('gphoto$timestamp');
                $photoModel->gphoto_imageVersion	= $entryObject->get('gphoto$imageVersion');
                $photoModel->gphoto_streamId        = $entryObject->get('streamId');

                $photoModel->exif_fstop				= $entryObject->get('exif$fstop');
                $photoModel->exif_make				= $entryObject->get('exif$make');
                $photoModel->exif_model				= $entryObject->get('exif$model');
                $photoModel->exif_exposure			= $entryObject->get('exif$exposure');
                $photoModel->exif_flash				= $entryObject->get('exif$flash');
                $photoModel->exif_focallength		= $entryObject->get('exif$focallength');
                $photoModel->exif_iso				= $entryObject->get('exif$iso');
                $photoModel->exif_time				= $entryObject->get('exif$time');
                $photoModel->exif_imageUniqueID		= $entryObject->get('exif$imageUniqueID');
                $photoModel->gml_pos				= $entryObject->get('gml$pos');

                $videoArray = $entryObject->get('gphoto$originalvideo');
                if (!is_null($videoArray)){
                    //VIDEO
                    $photoModel->is_video = true;
                    $photoModel->gphoto_originalvideo_width         = $videoArray['width'];
                    $photoModel->gphoto_originalvideo_height        = $videoArray['height'];
                    $photoModel->gphoto_originalvideo_type          = $videoArray['type'];
                    $photoModel->gphoto_originalvideo_duration      = (string)$videoArray['duration'];
                    $photoModel->gphoto_originalvideo_channels      = (string)$videoArray['channels'];
                    $photoModel->gphoto_originalvideo_samplingrate  = (string)$videoArray['samplingrate'];
                    $photoModel->gphoto_originalvideo_videoCodec    = $videoArray['videoCodec'];
                    $photoModel->gphoto_originalvideo_audioCodec    = $videoArray['audioCodec'];
                    $photoModel->gphoto_originalvideo_fps           = (string)$videoArray['fps'];
                    $photoModel->gphoto_videostatus                 = $entryObject->get('gphoto$videostatus');
                    $photoModel->sourceVideoOriginal                = $entryObject->get('sourceVideoOriginal');
                    $photoModel->sourceVideoThumb                   = $entryObject->get('sourceVideoThumb');
                    $photoModel->icon                               = $entryObject->get('sourceVideoIcon');
                }

                if ($behaviour == self::BEHAVIOUR_DO_NOT_SAVE){
                    $countSkipped++;
                    continue;
                }
                if (!$photoModel->save()){
                    $result['error'][] = ['model'=>$photoModel->attributes, 'errors'=>$photoModel->errors];
                    $countNotSaved++;
                } else {
                    $countSaved++;
                }
            }
            $result['entries'] = count($resp['feed']['entry']);
        } else {
            $result['error'][] = "No response or no feed";
        }

        $result['countSaved'] = $countSaved;
        $result['countNotSaved'] = $countNotSaved;
        $result['countSkipped'] = $countSkipped;
        return $result;
    }

    public function getMaxScanResults() {
        return $this->_maxScanResults;
    }

    function setMaxScanResults($maxScanResults) {
        $this->_maxScanResults = $maxScanResults;
    }


}
